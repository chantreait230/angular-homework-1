import { Status } from './../interface/status';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-total',
  templateUrl: './total.component.html',
  styleUrls: ['./total.component.css']
})
export class TotalComponent implements OnInit {

  constructor() { }

  @Input('data') status: Status[];

  @Input() regularPercent: number;
  @Input() dieselPercent: number;
  @Input() premuimPercent: number;

  ngOnInit(): void {
  }

}
