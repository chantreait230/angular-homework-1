import { Status } from './../interface/status';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor() { }

  logo: string ='https://telapetroleum.com/wp-content/uploads/2019/05/Logo-website-big.png';
  title: string = 'Annual Month Overall Data of TELA';

  randomUsed(used : any):any{
    return Math.floor(Math.random() * used);
  }

  regularPercent = (this.randomUsed(31) * 100) / 30;
  dieselPercent = (this.randomUsed(38) * 100) / 38;
  premuimPercent = (this.randomUsed(35) * 100) / 35;

  @Input() status: Status[] = [
    { used: this.randomUsed(31), available: 30 },
    { used: this.randomUsed(39), available: 38 },
    { used: this.randomUsed(35), available: 35 },
  ];
  
  getWeekRow(){
    return {
      name : 'week',
      regular :
      {
          used : this.randomUsed(31),
          available : 30
      },
      diesel :
      {
          used : this.randomUsed(39),
          available : 38
      },
      premium : 
      {
          used : this.randomUsed(26),
          available: 35
        }
    }
  }

  ngOnInit(): void {
  }

}
