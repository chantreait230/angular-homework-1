import { Week } from './../interface/week';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: '[app-week-row]',
  templateUrl: './week-row.component.html',
  styleUrls: ['./week-row.component.css']
})
export class WeekRowComponent implements OnInit {

  constructor() { }

  @Input() week: Week;

  ngOnInit(): void {
    console.log(this.week)
  }

}
